/*
    Program to output a string to Console    
    Author: Animesh Sinha
    Date: August 29, 2014
*/

#include <stdio.h>

int main(void)
{
    printf("Hello, world!\n");
}
