#include <cs50.h>
#include <stdio.h>

int main(void)
{
    // Input height of column between 0 and 23 and handle reprompting    
    int n;
    do
    {
        printf("Input the height of the coulmn between 0 and 23: ");
        n = GetInt();
        if (n > 23 || n < 0)
        {
            printf("Please follow the instructions on prompt\n");
        }
    }
    while(n > 23 || n < 0);
    
    /* Print n line using the first loop and
       print on the i th line 
           the n-i spaces and the i #s in each line 
       using the other two loops */
    for(int i = 1; i <= n; i++)
    {
        for(int j = 1; j <= (n - i); j++)
        {
            printf(" ");           
        }
        for(int k = 1; k <= (i + 1); k++)
        {
            printf("#");
        }
        printf("\n");
    }   
}
