#include <cs50.h>
#include <stdio.h>

int main(void)
{
    // Initialization and Inputs    
    double val_inp;
    int coins_ans = 0;
    /* Input of a number greater than 0 is to be take. 
       Reprompting will be handled here.
       Input will be taken as dollars: 6.79. 
       The $ sign is not to be written in input. */
    do
    {
        printf("Input the amount of money to be given out: ");
        val_inp = GetDouble();
        if (val_inp < 0) 
        {
            printf("Sorry, we need a positive amount\n");
        }
    }
    while(val_inp < 0);
    
    /* This section converts to dollars into cents and rounds of
       (not truncates) all values less than 1 cent */
    int val_cents = (int) (val_inp * 100);
    if ((val_inp * 100 - val_cents) > 50 || (val_cents - val_inp * 100) > 50) 
    {
        val_cents++;
    }
    
    // Counting algorithm
    while(val_cents >= 25)
    {
        coins_ans++;
        val_cents -= 25;
     }
    while(val_cents >= 10)
    {
        coins_ans++;
        val_cents -= 10;
    }
    while(val_cents >= 5)
    {
        coins_ans++;
        val_cents -= 5;
    }
    while(val_cents >= 1)
    {
        coins_ans++;
        val_cents -= 1;
    }
    
    // Displaying the Output
    printf("%d\n", coins_ans);    
}
