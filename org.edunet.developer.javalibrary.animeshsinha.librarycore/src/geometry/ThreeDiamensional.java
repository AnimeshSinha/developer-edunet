/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author Animesh
 */
public interface ThreeDiamensional {
    public double volume();
    public double surfaceArea();
}
