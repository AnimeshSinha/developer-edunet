/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author Animesh
 */
public class Cone implements ThreeDiamensional {
    public double r, h, l;
    public Cone(double radius, double height, double length)
    {
        r = radius;
        h = height;
        l = length;
        if(r == 0) r = Math.sqrt(l*l - h*h);
        if(h == 0) h = Math.sqrt(l*l - r*r);
        if(l == 0) l = Math.sqrt(h*h + r*r);
    }
    public Cone(double radius, double height)
    {
        r = radius;
        h = height;
        l = Math.sqrt(h*h + r*r);
    }
    public Cone(Cone cone)
    {
        r = cone.r;
        h = cone.h;
        l = cone.l;
    }
    public double volume()
    {
        return (1.0/3.0)*(Math.PI)*r*r*h;
    }
    public double surfaceArea()
    {
        return (1.0/3.0)*(Math.PI)*r*r*h;
    }
}