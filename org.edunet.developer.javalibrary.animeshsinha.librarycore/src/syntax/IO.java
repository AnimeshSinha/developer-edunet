/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntax;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import data.Date;

/**
 *
 * @author Animesh
 */
public class IO {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    public static void in(int var) throws IOException
    {
        var = Integer.parseInt(br.readLine());
    }
    public static void in(boolean var) throws IOException
    {
        var = Boolean.parseBoolean(br.readLine());
    }
    public static void in(float var) throws IOException
    {
        var = Float.parseFloat(br.readLine());
    }
    public static void in(double var) throws IOException
    {
        var = Double.parseDouble(br.readLine());
    }
    public static void in(long var) throws IOException
    {
        var = Long.parseLong(br.readLine());
    }
    public static void in(short var) throws IOException
    {
        var = Short.parseShort(br.readLine());
    }
    public static void in(byte var) throws IOException
    {
        var = Byte.parseByte(br.readLine());
    }
    public static void in(String var) throws IOException
    {
        var = br.readLine();
    }
    public static void out(String st)
    {
        System.out.print(st);
    }
    public static void out(Date date)
    {
        date.print("d-m-y");
    }
    public static void print(String val)
    {
        out(val);
        out("\n");
    }
    public static void print(Date val)
    {
        out(val);
        out("\n");
    }
}