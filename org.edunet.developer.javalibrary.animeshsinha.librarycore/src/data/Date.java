/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author Animesh
 */
public class Date {
    int date, month, year;
    public Date(int dd, int mm, int yyyy)
    {
        this.date = dd;
        this.month = mm;
        this.year = yyyy;
    }
    public Date(String value)
    {
        {
            String[] dt = value.split("-");
            this.date = Integer.parseInt(dt[0]);
            this.month = Integer.parseInt(dt[1]);
            this.year = Integer.parseInt(dt[2]);
        }
    }
    public Date(String value, String separator)
    {
        {
            String[] dt = value.split(separator);
            this.date = Integer.parseInt(dt[0]);
            this.month = Integer.parseInt(dt[1]);
            this.year = Integer.parseInt(dt[2]);
        }
    }
    public void print()
    {
        System.out.print(this.date + "-" + this.month + "-" + this.year);
    }
    public void print(String format)
    {
        if("d-m-y".equals(format))
            System.out.print(this.date + "-" + this.month + "-" + this.year);
        else
            System.out.print(this.date + "-" + this.month + "-" + this.year);
    }
    public String str()
    {
        return (this.date + "-" + this.month + "-" + this.year);
    }
    public String str(String format)
    {
        if("d-m-y".equals(format))
            return (this.date + "-" + this.month + "-" + this.year);
        else
            return (this.date + "-" + this.month + "-" + this.year);
    }
    public void add(Date dt)
    {
        this.date += dt.date;
        this.month += dt.month;
        this.year += dt.year;
    }
    public void add(String val)
    {
        Date dt = new Date(val);
        this.date += dt.date;
        this.month += dt.month;
        this.year += dt.year;
    }
    public void subtract(Date dt)
    {
        this.date -= dt.date;
        this.month -= dt.month;
        this.year -= dt.year;
    }
    public void subtract(String val)
    {
        Date dt = new Date(val);
        this.date -= dt.date;
        this.month -= dt.month;
        this.year -= dt.year;
    }
}