/**
 * recover.c
 *
 * Computer Science 50
 * Problem Set 5
 *
 * Recovers JPEGs from a forensic image.
 */
#include <stdio.h>

void positions(void);

int starts[51];
int size[50];

int main(void)
{  
    // store positions
    positions();
    
    // open input file and get variables initialized 
    char fileName[10];
    int data;

    FILE* inptr = fopen("card.raw", "r");
    if (inptr == NULL)
    {
        printf("Could not open card.raw.\n");
        return 1;
    }
    fseek(inptr, 512, SEEK_CUR);

    // start copying the 50 images; less than that while in debugging
    for(int image = 0; image < 50; image++)
    {
        // create the file
        if(image <= 9) sprintf(fileName, "00%d.jpg", image);
        else sprintf(fileName, "0%d.jpg", image);
        
        FILE* outptr = fopen(fileName, "w");
        if (outptr == NULL)
        {
            printf("Could not open %s.\n", fileName);
            return 1;
        }
        
        // copies the entire file
        for(int l = 0; l < size[image]; l++)
        {
            fread(&data, 1, 1, inptr);
            fwrite(&data, 1, 1, outptr);
        }
        
        // close the file
        fclose(outptr);
    }
}

void positions(void)
{
    // open input file 
    FILE* inptr = fopen("card.raw", "r");
    if (inptr == NULL)
    {
        printf("Could not open card.raw.\n");
        return;
    }

    // uses global array
    int loc = 0, dataa = 0x00, datab = 0x00, datac = 0x00, datad = 0x00, img_no = 0;
    while(img_no < 50)
    {
        fread(&dataa, 1, 1, inptr);
        fread(&datab, 1, 1, inptr);
        fread(&datac, 1, 1, inptr);
        fread(&datad, 1, 1, inptr);
        if(dataa == 0xff && datab == 0xd8 && datac == 0xff && (datad == 0xe0 || datad == 0xe1))
        {
            //@CHECK: printf("True: loc = %d = %x, image = %d\n", loc, loc, img_no);
            starts[img_no] = loc;
            img_no++;
        }
        loc += 512;
        fseek(inptr, 508, SEEK_CUR);
    }
    loc = starts[49];
    int exc = 0;
    while(feof(inptr) == 0)
    {
        exc = fread(&dataa, 1, 1, inptr);
        loc += 1;
    }
    starts[50] = loc + 511;
    
    // closes input file
    fclose(inptr);

    // array the sizes
    for(int image = 0; image <= 49; image++)
    {
        size[image] = starts[image + 1] - starts[image];
    }
}
