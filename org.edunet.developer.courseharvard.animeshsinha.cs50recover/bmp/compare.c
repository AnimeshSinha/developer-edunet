/**
 * copy.c
 *
 * Computer Science 50
 * Problem Set 5
 *
 * Copies a BMP piece by piece, just because.
 */
       
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"

int main(int argc, char* argv[])
{
    // ensure proper usage
    if (argc != 2)
    {
        printf("Usage: ./copy infile outfile\n");
        return 1;
    }

    // remember filenames
    char* infile = argv[1];

    // open input file 
    FILE* inptr = fopen(infile, "r");
    if (inptr == NULL)
    {
        printf("Could not open %s.\n", infile);
        return 2;
    }

    // read infile's BITMAPFILEHEADER
    BITMAPFILEHEADER bf;
    fread(&bf, sizeof(BITMAPFILEHEADER), 1, inptr);

    // read infile's BITMAPINFOHEADER
    BITMAPINFOHEADER bi;
    fread(&bi, sizeof(BITMAPINFOHEADER), 1, inptr);

    // ensure infile is (likely) a 24-bit uncompressed BMP 4.0
    if (bf.bfType != 0x4d42 || bf.bfOffBits != 54 || bi.biSize != 40 || 
        bi.biBitCount != 24 || bi.biCompression != 0)
    {
        fclose(inptr);
        fprintf(stderr, "Unsupported file format.\n");
        return 4;
    }

    // determine padding for scanlines
    // int padding =  (4 - (bi.biWidth * sizeof(RGBTRIPLE)) % 4) % 4;

    printf("bfType: %d \t\t\t", bf.bfType);
    printf("bfSize: %d \t\t\t", bf.bfSize);
    printf("bfReserved1: %d \t\t\t", bf.bfReserved1);
    printf("bfReserved2: %d \n", bf.bfReserved2);
    printf("bfOffBits: %d \t\t\t", bf.bfOffBits);
    printf("biSize: %d \t\t\t", bi.biSize);
    printf("biWidth: %d \t\t\t", bi.biWidth);
    printf("biHeight: %d \n", bi.biHeight);
    printf("biPlanes: %d \t\t\t", bi.biPlanes);
    printf("biBitCount: %d \t\t\t", bi.biBitCount);
    printf("biCompression: %d \t\t", bi.biCompression);
    printf("biSizeImage: %d \n", bi.biSizeImage);
    printf("biXPelsPerMeter: %d \t\t", bi.biXPelsPerMeter);
    printf("biYPelsPerMeter: %d \t\t", bi.biYPelsPerMeter);
    printf("biClrUsed: %d \t\t\t", bi.biClrUsed);
    printf("biClrImportant: %d \n", bi.biClrImportant);

    // close infile
    fclose(inptr);

    return 0;
}
