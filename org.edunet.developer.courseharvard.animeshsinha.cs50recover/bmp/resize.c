/**
 * copy.c
 *
 * Computer Science 50
 * Problem Set 5
 *
 * Copies a BMP piece by piece, just because.
 */

#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"

int main(int argc, char* argv[])
{
    // ensure proper usage
    if (argc != 4)
    {
        printf("Usage: ./resize scale infile outfile\n");
        return 1;
    }

    // read arguments - remember filenames and value of n
    int n = 0;
    for (int ck = 0; argv[1][ck] != '\0'; ck++)
        n = n * 10 + ((int)argv[1][ck] - 48);
    if (n < 1 || n > 100)
    {
        printf("Scale factor must be an integer between 1 and 100. Please retry\n");
        return 2;
    }
    
    char* infile = argv[2];
    char* outfile = argv[3];
    
    // open input file 
    FILE* inptr = fopen(infile, "r");
    if (inptr == NULL)
    {
        printf("Could not open %s.\n", infile);
        return 3;
    }

    // open output file
    FILE* outptr = fopen(outfile, "w");
    if (outptr == NULL)
    {
        fclose(inptr);
        fprintf(stderr, "Could not create %s.\n", outfile);
        return 4;
    }

    // read infile's BITMAPFILEHEADER
    BITMAPFILEHEADER bf;
    fread(&bf, sizeof(BITMAPFILEHEADER), 1, inptr);

    // read infile's BITMAPINFOHEADER
    BITMAPINFOHEADER bi;
    fread(&bi, sizeof(BITMAPINFOHEADER), 1, inptr);

    // ensure infile is (likely) a 24-bit uncompressed BMP 4.0
    if (bf.bfType != 0x4d42 || bf.bfOffBits != 54 || bi.biSize != 40 || 
        bi.biBitCount != 24 || bi.biCompression != 0)
    {
        fclose(outptr);
        fclose(inptr);
        fprintf(stderr, "Unsupported file format.\n");
        return 5;
    }

    BITMAPINFOHEADER bp = bi;
    bp.biHeight = bi.biHeight * n;
    bp.biWidth = bi.biWidth * n;

    // determine padding for scanlines
    int padding_old =  (4 - (bi.biWidth * sizeof(RGBTRIPLE)) % 4) % 4;
    int padding_new =  (4 - (bp.biWidth * sizeof(RGBTRIPLE)) % 4) % 4;
    
    bp.biSizeImage = abs(bp.biHeight) * (bp.biWidth * sizeof(RGBTRIPLE) + padding_new);
    bf.bfSize = bp.biSizeImage + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
    
    // write outfile's BITMAPFILEHEADER
    fwrite(&bf, sizeof(BITMAPFILEHEADER), 1, outptr);

    // write outfile's BITMAPINFOHEADER
    fwrite(&bp, sizeof(BITMAPINFOHEADER), 1, outptr);

    // iterate over infile's scanlines
    for (int i = 0, biHeight = abs(bi.biHeight); i < biHeight; i++)
    {
        RGBTRIPLE scanline[bp.biWidth]; 
        // iterate over pixels in scanline
        for (int j = 0; j < bi.biWidth; j++)
        {
            // temporary storage
            RGBTRIPLE triple;

            // read RGB triple from infile
            fread(&triple, sizeof(RGBTRIPLE), 1, inptr);

            for(int k = 0; k < n; k++)
            {
                scanline[j * n + k] = triple;
            }
        }
        
        // skip over padding, if any
        fseek(inptr, padding_old, SEEK_CUR);

        // write RGB triple to outfile
        for(int z = 0; z < n; z++)
        {
            for(int m = 0; m < bp.biWidth; m++)
            {
                fwrite(&scanline[m], sizeof(RGBTRIPLE), 1, outptr);
            }
            // then add padding back
            for (int k = 0; k < padding_new; k++)
            {
                fputc(0x00, outptr);
            }
        }        
    }

    // close infile
    fclose(inptr);

    // close outfile
    fclose(outptr);

    // that's all folks
    return 0;
}
