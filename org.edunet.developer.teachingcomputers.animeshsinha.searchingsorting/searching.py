import random
import sorting

def linearSearch(needle, haystack):
    for data in haystack:
        if needle == data:
            return True
    return False
    
def bisectionSearch(needle, haystack, steps = False, sort = False):
    if sort == "Bubble":
        haystack = sorting.bubbleSort(haystack)
    if sort == "Bubble":
        haystack = sorting.bubbleSort(haystack)
    if sort == "Selection":
        haystack = sorting.selectionSort(haystack)
    if sort == "Merge":
        haystack = sorting.mergeSort(haystack)
    elif sort == True or not sort == False:
        haystack = sorting.bubbleSort(haystack)
    amin = 0
    amax = len(haystack) - 1
    times = 1
    while not amin == amax and times <= 10:
        if steps == True:
            print "Searching: " + str(haystack[amin:amax+1]) + " on positions " + str(amin) + " to " + str(amax),
        times += 1
        amid = (amax + amin) / 2
        if steps == True:
            print "| Middle Positions: " + str(amid) + " and ",
        if haystack[amid] == needle:
            if steps == True:
                print str(haystack[amid]) + " = " + str(needle)
            return True
        elif haystack[amid] > needle:
            if steps == True:
                print str(haystack[amid]) + " > " + str(needle)
            amax = amid
        elif haystack[amid] < needle:
            if steps == True:
                print str(haystack[amid]) + " < " + str(needle)
            amin = amid
    return False
    
def randomSearch(needle, haystack, steps = False, maxtrials = 10000):
    for i in range(maxtrials):
        if needle == haystack[random.choice(range(0, len(haystack)))]:
            if steps == True:
                print "Trials involved: " + str(i)
            return True
    if steps == True:
        print "Trials involved: " + str(i)
    return False
