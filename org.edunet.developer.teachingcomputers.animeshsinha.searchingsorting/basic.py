import random

class Array:

    # Basic Array Related Functions
    
    data = []
        
    def __init__(self):
        self.data = []

    def __str__(self):
        return str(self.data)

    def generateData(self, size = 10, maximum = 100, minimum = 0, distribution='Linear'):
        for i in range(size):
            self.data.append(random.choice(range(minimum, maximum)))
        return self.data
        
    def clear(self):
        self.data = []
        
    def addData(self, data):
        self.data = self.data + data

    # Different Sorting Algorithms

    def bubbleSort(self, steps = False):
        successful = False
        if steps == True: print str(self.data) + '\n'
        while not successful:
            successful = True
            for i in range(len(self.data))[:-1]:
                if self.data[i] > self.data[i + 1]:
                    temp = self.data[i+1]
                    self.data[i+1] = self.data[i]
                    self.data[i] = temp
                    successful = False
                if steps == True: print self.data
            if steps == True: print

    def selectionSort(self, steps = False):
        for i in range(len(self.data)):
            smallest = self.data[i]
            for j in range(i, len(self.data)):
                if self.data[j] <= smallest:
                    smallest = self.data[j]
                    position = j
            temp = self.data[i]
            self.data[i] = smallest
            self.data[position] = temp
            if steps == True:
                print str(self.data) + ' Smallest number in the run = ' + str(smallest)

    def insertionSort(self, steps = False):
        for sortedUpto in range(0, len(self.data)):
            for i in range(sortedUpto):
                if self.data[sortedUpto] <= self.data[i]:
                    tempA = self.data[i]
                    self.data[i] = self.data[sortedUpto]
                    for j in range(i + 1, sortedUpto + 2):
                        tempB = tempA
                        if not j == len(self.data):
                            tempA = self.data[j]
                            self.data[j] = tempB
                    if not sortedUpto + 1 == len(self.data):
                        self.data[sortedUpto + 1] = tempA
                    break
            if steps == True:
                print str(self.data)
    
    def mergeSort(self):
        self.data = self.mergeSortHelper(self.data)
    def mergeSortHelper(self, data, steps = False):
        if len(data) == 1:
            return data
        data1 = self.mergeSortHelper(data[:len(data)/2])
        data2 = self.mergeSortHelper(data[len(data)/2:])
        data = []
        while data1 != [] and data2 != []:
            if data1[0] <= data2[0]:
                data.append(data1[0])
                data1 = data1[1:]
            else:
                data.append(data2[0])
                data2 = data2[1:]
        data = data + data1 + data2
        return data
        
    # Different Searching Algorithms
    
    def linearSearch(self, needle):
        for data in self.data:
            if needle == data:
                return True
        return False
        
    def bisectionSearch(self, needle, steps = False):
        amin = 0
        amax = len(self.data) - 1
        times = 1
        while not amin == amax and times <= 10:
            if steps == True:
                print "Searching: " + str(self.data[amin:amax+1]) + " on positions " + str(amin) + " to " + str(amax),
            times += 1
            amid = (amax + amin) / 2
            if steps == True:
                print "| Middle Positions: " + str(amid) + " and ",
            if self.data[amid] == needle:
                if steps == True:
                    print str(self.data[amid]) + " = " + str(needle)
                return True
            elif self.data[amid] > needle:
                if steps == True:
                    print str(self.data[amid]) + " > " + str(needle)
                amax = amid
            elif self.data[amid] < needle:
                if steps == True:
                    print str(self.data[amid]) + " < " + str(needle)
                amin = amid
        return False
        
    def randomSearch(self, needle, steps = False, maxtrials = 10000):
        for i in range(maxtrials):
            if needle == self.data[random.choice(range(0, len(self.data)))]:
                if steps == True:
                    print "Trials involved: " + str(i)
                return True
        if steps == True:
            print "Trials involved: " + str(i)
        return False