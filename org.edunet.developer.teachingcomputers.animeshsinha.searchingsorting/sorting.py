import random

def bubbleSort(data, steps = False):
    successful = False
    if steps == True: print str(data) + '\n'
    while not successful:
        successful = True
        for i in range(len(data))[:-1]:
            if data[i] > data[i + 1]:
                temp = data[i+1]
                data[i+1] = data[i]
                data[i] = temp
                successful = False
            if steps == True: print data
        if steps == True: print
    return data
    
def selectionSort(data, steps = False):
    for i in range(len(data)):
        smallest = data[i]
        for j in range(i, len(data)):
            if data[j] <= smallest:
                smallest = data[j]
                position = j
        temp = data[i]
        data[i] = smallest
        data[position] = temp
        if steps == True:
            print str(data) + ' Smallest number in the run = ' + str(smallest)
    return data

def insertionSort(data, steps = False):
    for sortedUpto in range(0, len(data)):
        for i in range(sortedUpto):
            if data[sortedUpto] <= data[i]:
                tempA = data[i]
                data[i] = data[sortedUpto]
                for j in range(i + 1, sortedUpto + 2):
                    tempB = tempA
                    if not j == len(data):
                        tempA = data[j]
                        data[j] = tempB
                if not sortedUpto + 1 == len(data):
                    data[sortedUpto + 1] = tempA
                break
        if steps == True:
            print str(data)
    return data

def mergeSort(data, steps = False):
    if len(data) == 1:
        return data
    data1 = mergeSort(data[:len(data)/2])
    data2 = mergeSort(data[len(data)/2:])
    data = []
    while data1 != [] and data2 != []:
        if data1[0] <= data2[0]:
            data.append(data1[0])
            data1 = data1[1:]
        else:
            data.append(data2[0])
            data2 = data2[1:]
    data = data + data1 + data2
    return data

def generateData(size = 10, maximum = 100, minimum = 0, distribution='Linear'):
    data = []
    for i in range(size):
        data.append(random.choice(range(minimum, maximum)))
    return data