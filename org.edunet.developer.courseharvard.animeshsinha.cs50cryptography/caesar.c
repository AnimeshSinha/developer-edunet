/* Program to convert a given plain text message to Caesar
   Cipher according to given key inputted at shell prompt
   Author: Animesh Sinha
   Date: August 31, 2014 */

#include <cs50.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main(int argc, string argv[])
{
    // Exception handling with command line arguments at prompt
    if (argc != 2)
    {
        printf("Error: Please input caeser command followed by 1 argument.\n");
        return 1;
    }
    char ltr, cph;
    int key = atoi(argv[1]);
    string msg = GetString();
    for(int i = 0; i < strlen(msg); i++)
    {
        ltr = msg[i];
        if (isalpha(ltr))
        {
            if (isupper(ltr))
            {
                cph = ltr - 65;
                cph = (cph + key) % 26 + 65;
            }
            else if (islower(ltr))
            {
                cph = ltr - 97;
                cph = (cph + key) % 26 + 97;
            }
         }
        else
        {
            cph = ltr;
        }
        printf("%c", cph);
    }
    printf("\n");
    return 0;
}
