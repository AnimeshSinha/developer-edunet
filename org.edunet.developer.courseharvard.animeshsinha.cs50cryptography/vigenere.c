/* Program to convert a given plain text message to Caesar
   Cipher according to given key inputted at shell prompt
   Author: Animesh Sinha
   Date: August 31, 2014 */

#include <cs50.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main(int argc, string argv[])
{
    // Exception handling with command line arguments at prompt
    if (argc != 2)
    {
        printf("Error: Please input caeser command followed by 1 argument.\n");
        return 1;
    }
    char ltr, cph, key;
    int key_pos = 0, flag = 0;
    string pass = argv[1];
    // Error with non alphabetical keywords
    for(int ct_m = 0; ct_m < strlen(pass); ct_m++)
    {
        if (isalpha(pass[ct_m]) == 0)
        {
            printf("Error. Invalid Keyword.\n");
            return 1;
        }
    }
    string msg = GetString();
    for(int i = 0; i < strlen(msg); i++)
    {
        // Selecting the key
        if (key_pos >= strlen(pass))
        {
            key_pos = 0;
        }
        key = pass[key_pos];
        if (isalpha(pass[key_pos]))
        {
            if (isupper(pass[key_pos]))
            {
                key = pass[key_pos] - 65;
            }
            else if (islower(pass[key_pos]))
            {
                key = pass[key_pos] - 97;
            }
            flag = 0;
        }
        ltr = msg[i];
        // Calculating the cipher text letters using the key
        if (isalpha(ltr))
        {
            if (isupper(ltr))
            {
                cph = ltr - 65;
                cph = (cph + key) % 26 + 65;
            }
            else if (islower(ltr))
            {
                cph = ltr - 97;
                cph = (cph + key) % 26 + 97;
            }
            key_pos++;
        }
        else
        {
            cph = ltr;
        }
        printf("%c", cph);
    }
    // Successful Execution
    printf("\n");
    return 0;
}
