//
// breakout.c
//
// Computer Science 50
// Problem Set 4
//

// standard libraries
#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// Stanford Portable Library
#include "gevents.h"
#include "gobjects.h"
#include "gwindow.h"

// height and width of game 's window in pixels
#define HEIGHT 600
#define WIDTH 400

// number of rows of bricks
#define ROWS 5

// number of columns of bricks
#define COLS 10

// radius of ball in pixels
#define RADIUS 10

// lives
#define LIVES 3

#define PADDLE_SPEED 7.5

// prototypes
void initBricks(GWindow window);
GOval initBall(GWindow window);
GRect initPaddle(GWindow window);
GLabel initScoreboard(GWindow window);
void updateScoreboard(GWindow window, GLabel label, int points);
GObject detectCollision(GWindow window, GOval ball);
void dropBall(int* lives, GLabel label, GOval ball, GWindow window, int points);

// global objects
GRect brick[ROWS * COLS];


int main(void)
{
    // seed pseudorandom number generator
    srand48(time(NULL));

    // instantiate window
    GWindow window = newGWindow(WIDTH, HEIGHT);

    // instantiate bricks
    initBricks(window);

    // instantiate ball, centered in middle of window
    GOval ball = initBall(window);
    double horizontal_velocity = 2 * drand48() +0.25;
    double vertical_velocity = -2.5;
    
    // instantiate paddle, centered at bottom of window
    GRect paddle = initPaddle(window);

    // instantiate scoreboard, centered in middle of window, just above ball
    GLabel label = initScoreboard(window);

    // number of bricks initially
    int bricks = COLS * ROWS;

    // number of lives initially
    int lives = LIVES;

    // number of points initially
    int points = 0;
        
    waitForClick();

    // keep playing until game over
    while (lives > 0 && bricks > 0)
    {
        // make the paddle move, responding to keypreses
        GEvent event = getNextEvent(MOUSE_EVENT);
        if (event != NULL)
        {
            if (getEventType(event) == MOUSE_MOVED)
            {
                double x = getX(event) - getWidth(paddle) / 2;
                setLocation(paddle, x, HEIGHT * 0.9);
            }
        }

        // move the ball and have it robound off edges, brick and paddle
        move(ball, horizontal_velocity, vertical_velocity);
        
        if (getX(ball) + getWidth(ball) >= getWidth(window)||getX(ball) <= 0)
            horizontal_velocity = -horizontal_velocity;
        if (getY(ball) + getHeight(ball) >= getHeight(window) || getY(ball) <= 0)
            vertical_velocity = -vertical_velocity;
        if (detectCollision(window, ball) == paddle && vertical_velocity > 0)
            vertical_velocity = -vertical_velocity;
        for(int k = 0; k < 50; k++)
        {
            if (detectCollision(window, ball) == brick[k])
            {
                vertical_velocity = -vertical_velocity;
                removeGWindow(window, brick[k]);
                bricks--;
                points++;
                updateScoreboard(window, label, points);
            }
        }
        pause(10);

        // check if ball was dropped and handle it
        if (getY(ball) >= HEIGHT - 2 * RADIUS)
        {
            dropBall(&lives, label, ball, window, points);
        }
        
    }
    waitForClick();
    
    // game over
    closeGWindow(window);
    return 0;
}

/**
 * Initializes window with a grid of bricks.
 */
void initBricks(GWindow window)
{
    int index = 0;
    for(int i = 0; i < ROWS; i++)
    {
        for(int j = 0; j < COLS; j++)
        {
            index = 10 * i + j;
            brick[index] = newGRect((WIDTH/COLS) * j + (WIDTH/COLS) * 0.05, 25 + 20 * i , (WIDTH/COLS) * 0.9, 15);
            setFilled(brick[index], 1);
            if(i == 0) setColor(brick[index], "RED");
            else if(i == 1) setColor(brick[index], "ORANGE");
            else if(i == 2) setColor(brick[index], "YELLOW");
            else if(i == 3) setColor(brick[index], "GREEN");
            else if(i == 4) setColor(brick[index], "BLUE");
            add(window, brick[index]);
        }
    }
}

/**
 * Instantiates ball in center of window.  Returns ball.
 */
GOval initBall(GWindow window)
{
    GOval ball = newGOval(WIDTH/2 - RADIUS, HEIGHT/2 - RADIUS, 2 * RADIUS, 2 * RADIUS);
    setFilled(ball, 1);
    add(window, ball);
    return ball;
}

/**
 * Instantiates paddle in bottom-middle of window.
 */
GRect initPaddle(GWindow window)
{
    GRect paddle = newGRect(WIDTH/2 - WIDTH/10, HEIGHT * 0.9, WIDTH/5, 10);
    setFilled(paddle, 1);
    add(window, paddle);
    return paddle;
}

/**
 * Instantiates, configures, and returns label for scoreboard.
 */
GLabel initScoreboard(GWindow window)
{
    int points = 0;
    GLabel score = newGLabel("");
    setFont(score, "SansSerif-50");
    add(window, score);
    char s[12];
    sprintf(s, "%i", points);
    setLabel(score, s);
    double x = (getWidth(window) - getWidth(score)) / 2;
    double y = (getHeight(window) - getHeight(score)) / 2;
    setLocation(score, x, y);
    setColor(score, "GRAY");
    return score;
}

/**
 * Updates scoreboard's label, keeping it centered in window.
 */
void updateScoreboard(GWindow window, GLabel label, int points)
{
    // update label
    char s[12];
    sprintf(s, "%i", points);
    setFont(label, "SansSerif-50");
    setLabel(label, s);

    // center label in window
    double x = (getWidth(window) - getWidth(label)) / 2;
    double y = (getHeight(window) - getHeight(label)) / 2;
    setLocation(label, x, y);
}

/**
 * Detects whether ball has collided with some object in window
 * by checking the four corners of its bounding box (which are
 * outside the ball's GOval, and so the ball can't collide with
 * itself).  Returns object if so, else NULL.
 */
GObject detectCollision(GWindow window, GOval ball)
{
    // ball's location
    double x = getX(ball);
    double y = getY(ball);

    // for checking for collisions
    GObject object;

    // check for collision at ball's top-left corner
    object = getGObjectAt(window, x, y);
    if (object != NULL)
    {
        return object;
    }

    // check for collision at ball's top-right corner
    object = getGObjectAt(window, x + 2 * RADIUS, y);
    if (object != NULL)
    {
        return object;
    }

    // check for collision at ball's bottom-left corner
    object = getGObjectAt(window, x, y + 2 * RADIUS);
    if (object != NULL)
    {
        return object;
    }

    // check for collision at ball's bottom-right corner
    object = getGObjectAt(window, x + 2 * RADIUS, y + 2 * RADIUS);
    if (object != NULL)
    {
        return object;
    }

    // no collision
    return NULL;
}

void dropBall(int* lives, GLabel label, GOval ball, GWindow window, int points)
{
    if(*lives > 1)
    {
        pause(500);
        setLocation(ball, WIDTH/2 - RADIUS, HEIGHT/2 - RADIUS);
        waitForClick();
        updateScoreboard(window, label, points);
        (*lives)--;
        pause(1000);
    }
    else
    {
        pause(500);
        (*lives)--;
        pause(1000);
    }
}
