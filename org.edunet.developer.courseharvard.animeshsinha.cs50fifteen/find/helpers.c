/**
 * helpers.c
 *
 * Computer Science 50
 * Problem Set 3
 *
 * Helper functions for Problem Set 3.
 */
       
#include <cs50.h>
#include <stdio.h>

#include "helpers.h"

/**
 * Returns true if value is in array of n values, else false.
 */
bool search(int value, int values[], int n) 
// @Animesh: value = needle, values[] = haystack, n = size of haystack 
{
    // @Animesh: Logrithmic Search
    int r_max = n - 1, r_min = 0, pos = 0; 
    do
    {
        pos = (r_max + r_min) / 2;
// #debug: printf("\n val = %d and pos = %d, min = %d and max = %d", values[pos], pos, r_min, r_max);
        if (values[pos] > value)
        {
            r_max = pos - 1;
        }
        else if (values[pos] < value)
        {
            r_min = pos + 1;
        }
        else if (values[pos] == value)
        {
            return true;
        }
        if(r_max == r_min && value ==values[r_max])
        {
            return true;
        }
    } 
    while(r_max != r_min);
    
    return false;
}

/**
 * Sorts array of n values.
 */
void sort(int values[], int n)
{
    // @Animesh: Selection Sort
    int val_min = values[0], pos_min = 0, temp = 0;
    for(int i = 0; i < n; i++)
    {
        val_min = values[i];
        pos_min = i;
        for(int j = i; j < n; j++)
        {
            if (values[j] < val_min)
            {
                val_min = values[j];
                pos_min = j;
            }
        }
// #debug: printf("%d ", val_min);
        temp = values[i];
        values[i] = val_min;
        values[pos_min] = temp;
    }
    return;
}
