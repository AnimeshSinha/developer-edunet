/****************************************************************************
 * dictionary.c
 *
 * Computer Science 50
 * Problem Set 6
 *
 * Implements a dictionary's functionality.
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include "dictionary.h"

#define MAXIMUM 10000000

typedef struct node
{
    struct node* next[27] = {NULL};
} node;

/**
 * Returns the hash value of a word - pass first letter of word to it.
 */
int hasher(char key)
{
    int index = 0;
    if ((int) key >= 65 && (int) key <= 90)
    {
        index = (int) key - 65;
    }
    else if ((int) key >= 97 && (int) key <= 122)
    {
        index = (int) key - 97;
    }
    else
    {
        index = 27;
    }
    return index;
}

/**
 * Reads the next word from the dictionary passed to it and returns it.
 */
char* getDict(FILE* fp_dict)
{
    // prepare to spell-check
    int index = 0;
    char* word = malloc(sizeof(char) * (LENGTH + 1));

    // spell-check each word in text
    for (int chr = fgetc(fp_dict); chr != EOF; chr = fgetc(fp_dict))
    {
        // allow only alphabetical characters and apostrophes
        if (isalpha(chr) || (chr == '\'' && index > 0))
        {
            // append character to word
            word[index] = chr;
            index++;

            // ignore alphabetical strings too long to be words
            if (index > LENGTH)
            {
                // consume remainder of alphabetical string
                while ((chr = fgetc(fp_dict)) != EOF && isalpha(chr))
                {
                }
                return NULL;
            }
        }

        // we must have found a whole word
        else if (index > 0)
        {
            // terminate current word
            word[index] = '\0';
            return word;
        }
    }
    return NULL;
}

/**
 * Returns true if word is in dictionary else false.
 */
bool check(const char* word)
{
    int code = 0;
    while(count < MAXIMUM)
    {
        word = getDict(dict);
        if (word == NULL)
        {
            break;
        }
        int* cur_ptr = malloc(sizeof(node));
        for(int i = 0; i <= strlen(new_node->word); i++)
        {
            code = hasher(word[i]);
            if (cur_ptr->next[code] == NULL)
            {
                cur_ptr->next[code] = malloc(sizeof(node));
            }
            cur_ptr = cur_ptr->next[code];
        }
    }
}

/**
 * Loads dictionary into memory.  Returns true if successful else false.
 */
bool load(const char* dictionary)
{
    // open the file
    FILE* dict = fopen(dictionary, "r");
    if (dict == NULL)
    {
        return false;
    }
    int code = 0;
    char* word;
    while(count < MAXIMUM)
    {
        word = getDict(dict);
        if (word == NULL)
        {
            break;
        }
        int* cur_ptr = malloc(sizeof(node));
        for(int i = 0; i <= strlen(new_node->word); i++)
        {
            code = hasher(word[i]);
            if (cur_ptr->next[code] == NULL)
            {
                cur_ptr->next[code] = malloc(sizeof(node));
            }
            cur_ptr = cur_ptr->next[code];
        }
        count++;
    }
    return true;
}

/**
 * Returns number of words in dictionary if loaded else 0 if not yet loaded.
 */
unsigned int size(void)
{
    return count;
}

/**
 * Unloads dictionary from memory.  Returns true if successful else false.
 */
bool unload(void)
{
    node* temp = NULL;
    int* t = malloc(4);
    free(t);
    for (int i = 0; i < 26; i++)
    {
        node* cur_node = heads[i];
        while(cur_node != NULL)
        {
            temp = cur_node;
            cur_node = cur_node->next;
            free(temp);
        }
    }
    return true;
}

/*********************************************************/
/* TODO LIST: LIST OF IMPROVEMENTS TO BE MADE TO THE CODE
   1. Change the hasher() function to 2 letter hashing
   2. Change the heads[] array and other code accordingly
   3. Revisit size and try eleminating global variable   
   4. Try to replace getDict() with fscanf()             
   5. Be successfuin unloading the dictionary            */
/*********************************************************/
