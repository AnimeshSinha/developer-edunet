/****************************************************************************
 * dictionary.c
 *
 * Computer Science 50
 * Problem Set 6
 *
 * Implements a dictionary's functionality.
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include "dictionary.h"

#define MAXIMUM 10000000

typedef struct node
{
    char* word;
    struct node* next;
} node;

node* heads[26] = {NULL};
int count = 0;

/**
 * Reads the next word from the dictionary passed to it and returns it.
 */
char* getDict(FILE* fp_dict)
{
    // prepare to spell-check
    int index = 0;
    char* word = malloc(sizeof(char) * (LENGTH + 1));

    // spell-check each word in text
    for (int chr = fgetc(fp_dict); chr != EOF; chr = fgetc(fp_dict))
    {
        // allow only alphabetical characters and apostrophes
        if (isalpha(chr) || (chr == '\'' && index > 0))
        {
            // append character to word
            word[index] = chr;
            index++;

            // ignore alphabetical strings too long to be words
            if (index > LENGTH)
            {
                // consume remainder of alphabetical string
                while ((chr = fgetc(fp_dict)) != EOF && isalpha(chr))
                {
                }
                return NULL;
            }
        }

        // we must have found a whole word
        else if (index > 0)
        {
            // terminate current word
            word[index] = '\0';
            return word;
        }
    }
    return NULL;
}

/**
 * Returns the hash value of a word - pass first letter of word to it.
 */
int hasher(char key)
{
    int index = 0;
    if ((int) key >= 65 && (int) key <= 90)
    {
        index = (int) key - 65;
    }
    else if ((int) key >= 97 && (int) key <= 122)
    {
        index = (int) key - 97;
    }
    else
    {
        index = (int) key % 26;
    }
    return (index % 26);
}

/**
 * Returns true if words are same irrespective of case else false.
 */
bool strcmpi(char* word1, const char* word2)
{
    if (strlen(word1) != strlen(word2))
    {
        return false;
    }
    for(int i = 0; i < strlen(word1); i++)
    {
        if (tolower(word1[i]) != tolower((int) word2[i]))
        {
            return false;
        }
    }
    return true;
}

/**
 * Returns true if word is in dictionary else false.
 */
bool check(const char* word)
{
    int index = hasher(word[0]);
    node* cur_node = heads[index];
    while(cur_node != NULL)
    {
        if (!strcmp(cur_node->word, word))
        {
            return true;
        }
        else if(strcmpi(cur_node->word, word))
        {
            return true;
        }
        cur_node = cur_node->next;
    }
    return false;
}

/**
 * Loads dictionary into memory.  Returns true if successful else false.
 */
bool load(const char* dictionary)
{
    // open the file
    FILE* dict = fopen(dictionary, "r");
    if (dict == NULL)
    {
        return false;
    }
    int index = 0;
    while(count < MAXIMUM)
    {    
        // malloc space for the node struct
        node* new_node = malloc(sizeof(node));
        // load word from file to structure
        new_node->word = getDict(dict);
        if (new_node->word == NULL)
        {
            break;
        }
        // append the node according to the hasher function
        index = hasher(new_node->word[0]);
        new_node->next = heads[index];
        heads[index] = new_node;
        // DEBUG: printf("Loading %s, Hash = %d\n", new_node->word, index);
        count++;
    }
    return true;
}

/**
 * Returns number of words in dictionary if loaded else 0 if not yet loaded.
 */
unsigned int size(void)
{
    return count;
}

/**
 * Unloads dictionary from memory.  Returns true if successful else false.
 */
bool unload(void)
{
    node* temp = NULL;
    int* t = malloc(4);
    free(t);
    for (int i = 0; i < 26; i++)
    {
        node* cur_node = heads[i];
        while(cur_node != NULL)
        {
            temp = cur_node;
            cur_node = cur_node->next;
            free(temp);
        }
    }
    return true;
}

/*********************************************************/
/* TODO LIST: LIST OF IMPROVEMENTS TO BE MADE TO THE CODE
   1. Change the hasher() function to 2 letter hashing
   2. Change the heads[] array and other code accordingly
   3. Revisit size and try eleminating global variable   
   4. Try to replace getDict() with fscanf()             
   5. Be successfuin unloading the dictionary            */
/*********************************************************/
